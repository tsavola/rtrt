/* $Id: render.c,v 1.2 2008-09-03 09:12:35 timo Exp $ */

#include <stdlib.h>
#include <math.h>
#include <SDL.h>
#include "config.h"
#include "render.h"

static void normalize(vector_t *v)
{
	const double f = 1.0 / sqrt(v->x * v->x + v->y * v->y + v->z * v->z);
	v->x *= f;
	v->y *= f;
	v->z *= f;
}

static double distance(const double b,
		       const double c)
{
	double t = -1.0;

	const double d = b * b - 4 * c;
	if (d >= 0.0) {
		const double e = sqrt(d);
		t = (- b - e) * 0.5;
		if (t < 0.0) {
			t = (- b + e) * 0.5;
		}
	}

	return t;
}

static double intersect(const object_t *object,
			const ray_t *ray)
{
	const vector_t *r0 = &ray->origin;
	const vector_t *rd = &ray->direction;
	const vector_t *s0 = &object->origin;
	double b, c, cx, cy, cz;

	b = 2 * (rd->x * (r0->x - s0->x) +
	         rd->y * (r0->y - s0->y) +
	         rd->z * (r0->z - s0->z));

	cx = r0->x - s0->x;
	cy = r0->y - s0->y;
	cz = r0->z - s0->z;
	c = cx * cx + cy * cy + cz * cz - object->radius * object->radius;

	return distance(b, c);
}

static double intersect0(const object_t *object,
			 const ray_t *ray)
{
	const vector_t *rd = &ray->direction;
	const vector_t *s0 = &object->origin;
	double b, c;

	b = -2 * (rd->x * s0->x + rd->y * s0->y + rd->z * s0->z);

	c = s0->x * s0->x +
	    s0->y * s0->y +
	    s0->z * s0->z -
	    object->radius * object->radius;

	return distance(b, c);
}

static Uint32 get_color(const object_t *object,
			object_t **objects,
			light_t **lights,
			const ray_t *ray,
			const double t)
{
	const vector_t *rd = &ray->direction;
	const vector_t *s0 = &object->origin;
	double f;
	double is_x, is_y, is_z;
	double sn_x, sn_y, sn_z;
	double r = AMBIENT_SHADE;
	double g = AMBIENT_SHADE;
	double b = AMBIENT_SHADE;
	ray_t shray;

	is_x = rd->x * t;
	is_y = rd->y * t;
	is_z = rd->z * t;

	f = 1.0 / object->radius;
	sn_x = (is_x - s0->x) * f;
	sn_y = (is_y - s0->y) * f;
	sn_z = (is_z - s0->z) * f;

	shray.origin.x = is_x + KLUDGE_OFFSET * sn_x;
	shray.origin.y = is_y + KLUDGE_OFFSET * sn_y;
	shray.origin.z = is_z + KLUDGE_OFFSET * sn_z;

	while (*lights != 0) {
		object_t **objp;
		const light_t *light = *lights++;

		shray.direction.x = light->origin.x - shray.origin.x;
		shray.direction.y = light->origin.y - shray.origin.y;
		shray.direction.z = light->origin.z - shray.origin.z;
		normalize(&shray.direction);

		for (objp = objects; *objp != 0; objp++) {
			if (intersect(*objp, &shray) > 0.0) {
				goto skip;
			}
		}

		{
			const double product =
				sn_x * shray.direction.x +
				sn_y * shray.direction.y +
				sn_z * shray.direction.z;

			if (product >= 0.0) {
				r += product * light->color.r;
				g += product * light->color.g;
				b += product * light->color.b;
			}
		}
skip:
		;
	}

	if (r > 1.0) {
		r = 1.0;
	}
	if (g > 1.0) {
		g = 1.0;
	}
	if (b > 1.0) {
		b = 1.0;
	}

	return ((Uint32) (r * 255.0) << 16) |
	       ((Uint32) (g * 255.0) << 8) |
	       (Uint32) (b * 255.0);
}

static Uint32 trace_ray(object_t **objects,
			light_t **lights,
			const ray_t *ray)
{
	object_t **objp;
	object_t *closest = 0;
	float distance = 0.f;

	for (objp = objects; *objp != 0; objp++) {
		const double dist = intersect0(*objp, ray);
		if (dist > 0.f && (closest == 0 || dist < distance)) {
			closest = *objp;
			distance = dist;
		}
	}

	if (closest != 0) {
		return get_color(closest, objects, lights, ray, distance);
	} else {
		return 0;
	}
}

static void render_raw(Uint32 *buffer,
		       const Uint32 pitch,
		       object_t **objects,
		       light_t **lights)
{
	ray_t ray;
	int x, y;

	ray.origin.x = 0.f;
	ray.origin.y = 0.f;
	ray.origin.z = 0.f;

	for (y = 0; y < HEIGHT; y++) {
		Uint32 *buf;

		buf = buffer + pitch * y;

		for (x = 0; x < WIDTH; x++) {
			ray.direction.x = x - WIDTH / 2;
			ray.direction.y = - (y - HEIGHT / 2);
			ray.direction.z = HEIGHT / 2;
			normalize(&ray.direction);

			*buf++ = trace_ray(objects, lights, &ray);
		}
	}
}

#if OFFSCREEN_BUFFER
static Uint32 *offscreen = 0;
#endif

void init_render(void)
{
#if OFFSCREEN_BUFFER
	if (offscreen == 0) {
		offscreen = malloc(WIDTH * HEIGHT * 4);
	}
#endif
}

void shutdown_render(void)
{
#if OFFSCREEN_BUFFER
	if (offscreen != 0) {
		free(offscreen);
		offscreen = 0;
	}
#endif
}

void render(SDL_Surface *screen,
	    object_t **objects,
	    light_t **lights)
{
#if OFFSCREEN_BUFFER
	Uint32 *off;
	int x, y;

	render_raw(offscreen, WIDTH, objects, lights);
#endif

	if (SDL_MUSTLOCK(screen) && SDL_LockSurface(screen) < 0) {
		fprintf(stderr, "SDL_LockSurface: %s\n",
			SDL_GetError());
		exit(1);
	}

#if OFFSCREEN_BUFFER
	off = offscreen;

	for (y = 0; y < HEIGHT; y++) {
		Uint32 *buf;

		buf = (Uint32 *) ((Uint8 *) screen->pixels +
				  screen->pitch * y);

		for (x = 0; x < WIDTH; x++) {
			*buf++ = *off++;
		}
	}
#else
	render_raw((Uint32 *) screen->pixels, screen->pitch >> 2,
		   objects, lights);
#endif

	if (SDL_MUSTLOCK(screen)) {
		SDL_UnlockSurface(screen);
	}

	SDL_UpdateRect(screen, 0, 0, WIDTH, HEIGHT);
}
