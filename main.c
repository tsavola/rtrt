/* $Id: main.c,v 1.1 2003-08-28 15:52:25 timo Exp $ */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sys/wait.h>
#include <math.h>
#include <SDL.h>
#include "config.h"
#include "render.h"

static object_t object1 = { { 0.f, -5.f,  19.f },  10.f };
static object_t object2 = { { 0.f,  5.f,  21.f },  10.f };
static object_t object3 = { { 0.f,  0.f, 120.f }, 100.f };

static object_t *objects[] = {
	&object1,
	&object2,
	&object3,
	0
};

static light_t light1 = { { -20.f,  20.f, 15.f }, { 0.7f, 1.0f, 1.0f } };
static light_t light2 = { {  20.f, -20.f, 16.f }, { 1.0f, 0.9f, 0.8f } };
static light_t light3 = { {  10.f,  80.f, 10.f }, { 0.5f, 0.5f, 0.5f } };
static light_t light4 = { {   0.f,   0.f, 10.f }, { 0.6f, 0.4f, 0.4f } };

static light_t *lights[] = {
	&light1,
	&light2,
	&light3,
//	&light4,
	0
};

static int frames = 0;
static clock_t start_time = 0;

static void exit_hook(void)
{
	clock_t end_time;
	double seconds;

	end_time = clock();
	seconds = (double) (end_time - start_time) / CLOCKS_PER_SEC;

	SDL_Quit();
	shutdown_render();

	printf("BogoFPS: %f\n", frames / seconds);
}

static void signal_hook(int signal)
{
	exit(0);
}

int main(void)
{
	SDL_Surface *screen;
	double angle1 = 0.0;
	double angle2 = 1.0;
	double angle3 = 2.0;

	if (atexit(exit_hook) != 0) {
		fprintf(stderr, "atexit failed\n");
		return 1;
	}
	signal(SIGINT, signal_hook);
	signal(SIGTERM, signal_hook);

	init_render();

	if (SDL_Init(SDL_INIT_VIDEO) == -1) {
		fprintf(stderr, "SDL_Init: %s\n", SDL_GetError());
		exit(1);
	}

	screen = SDL_SetVideoMode(WIDTH, HEIGHT, 32,
				  SDL_SWSURFACE /* | SDL_FULLSCREEN */ );
	if (screen == 0) {
		fprintf(stderr, "SDL_SetVideoMode: %s\n", SDL_GetError());
		exit(1);
	}
	if (screen->format->BytesPerPixel != 4) {
		fprintf(stderr, "BytesPerPixel is not 4\n");
		exit(1);
	}

	start_time = clock();

	while (1 /* frames < 128 */ ) {
		SDL_Event event;
		const double pos = sin(angle1) * 15.0;
		object1.origin.x = pos;
		object2.origin.x = -pos;

		object1.origin.z = cos(angle2) * 5.0 + 20.0;

		light2.origin.z = sin(angle3) * -10.0 + 10.0;

		{
			const double f = cos(angle3) + 1.0;
			light4.color.r = f * 0.3;
			light4.color.g = f * 0.2;
			light4.color.b = f * 0.2;
		}

		angle1 += 0.04;
		if (angle1 >= 2 * M_PI) {
			angle1 = 0.0;
		}

		angle2 += 0.05;
		if (angle2 >= 2 * M_PI) {
			angle2 = 0.0;
		}

		angle3 += 0.07;
		if (angle3 >= 2 * M_PI) {
			angle3 = 0.0;
		}

		render(screen, objects, lights);

		frames++;

		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				return 0;
			}
			if (event.type == SDL_KEYDOWN &&
			    event.key.keysym.sym == SDLK_ESCAPE) {
				return 0;
			}
		}
	}

	return 0;
}
