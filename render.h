/* $Id: render.h,v 1.1 2003-08-28 15:52:25 timo Exp $ */

#ifndef RENDER_H
#define RENDER_H

#include <SDL.h>

typedef struct {
	float x;
	float y;
	float z;
} vector_t;

typedef struct {
	float r;
	float g;
	float b;
} color_t;

typedef struct {
	vector_t origin;
	float radius;
} object_t;

typedef struct {
	vector_t origin;
	color_t color;
} light_t;

typedef struct {
	vector_t origin;
	vector_t direction;
} ray_t;

extern void init_render(void);

extern void shutdown_render(void);

extern void render(SDL_Surface *,
		   object_t **,
		   light_t **);

#endif
