# $Id: Makefile,v 1.1 2003-08-28 15:52:25 timo Exp $

TARGET	=	out
OBJS	=	main.o render.o

CC      =	gcc

CFLAGS  =	-Wall -Wmissing-prototypes -O3 -fomit-frame-pointer -pipe \
		-fno-strict-aliasing -fno-common -mpreferred-stack-boundary=2 \
		-march=i686 -mmmx -msse -m3dnow `sdl-config --cflags`
LDFLAGS =	-lm `sdl-config --libs`

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $*.c

clean:
	rm -f $(TARGET) $(OBJS) *~

main.o:		main.c render.h config.h
render.o:	render.c render.h config.h
