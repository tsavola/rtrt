/* $Id: config.h,v 1.1 2003-08-28 15:52:25 timo Exp $ */

#ifndef CONFIG_H
#define CONFIG_H

#define WIDTH  640
#define HEIGHT 480

#define OFFSCREEN_BUFFER 0

#define KLUDGE_OFFSET    0.00001

#define AMBIENT_SHADE    0.02

#endif
